
-- Дамп структуры базы данных payments
CREATE DATABASE IF NOT EXISTS `payments` 

-- Дамп структуры для таблица payments.accounts
CREATE TABLE IF NOT EXISTS payments.accounts (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(45) NOT NULL,
  `amount` double NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы payments.accounts: ~11 rows (приблизительно)
INSERT IGNORE INTO payments.accounts (`aid`, `currency`, `amount`, `status`) VALUES
	(1, 'ADMIN', 0, 0),
	(2, 'USD', 0, 0),
	(3, 'BYR', 99010753.77, 0),
	(4, 'USD', 1500, 0);

-- Дамп структуры для таблица payments.cards
CREATE TABLE IF NOT EXISTS payments.cards (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `validity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `aid` int(11) NOT NULL,
  PRIMARY KEY (`cid`),
  KEY `fk_Cards_Accounts1` (`aid`),
  CONSTRAINT `FK_cards_accounts` FOREIGN KEY (`aid`) REFERENCES `accounts` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп структуры для таблица payments.users
CREATE TABLE IF NOT EXISTS payments.users (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `aid` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `access_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `aid` (`aid`),
  KEY `fk_Clients_Accounts1` (`aid`),
  CONSTRAINT `FK_users_accounts` FOREIGN KEY (`aid`) REFERENCES `accounts` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы payments.users: ~5 rows (приблизительно)
INSERT IGNORE INTO payments.users (`uid`, `first_name`, `last_name`, `aid`, `login`, `password`, `access_level`) VALUES
	(1, 'Алексей', 'Худницкий', 1, 'admin', 'admin', 1),
	(2, 'Алексей', 'Худницкий', 2, 'client', 'client', 0),
	(3, 'Андрей', 'Трошкин', 3, 'client2', 'client', 0),
	(4, 'Дмитрий', 'Рогатовский', 4, '11', '11', 0);

-- Дамп структуры для таблица payments.operations
CREATE TABLE IF NOT EXISTS payments.operations (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `description` varchar(45) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`oid`),
  KEY `fk_Clients_has_Accounts_Clients1` (`uid`),
  KEY `fk_Clients_has_Accounts_Accounts1` (`aid`),
  CONSTRAINT `FK_operations_accounts` FOREIGN KEY (`aid`) REFERENCES `accounts` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_operations_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы payments.operations: ~47 rows (приблизительно)
INSERT IGNORE INTO payments.operations (`oid`, `uid`, `aid`, `amount`, `description`, `date`) VALUES
	(55, 2, 3, 0, 'Блокировка счета', '2015-12-08 13:08:53'),
	(56, 2, 3, 1000.23, 'Платеж', '2015-12-08 13:39:01'),
	(59, 2, 3, 1234567, 'Платеж', '2015-12-08 13:56:40'),
	(60, 2, 3, 12, 'Пополнение счета', '2015-12-08 13:56:52'),
	(61, 2, 3, 0, 'Блокировка счета', '2015-12-08 13:56:54'),
	(62, 2, 3, 564, 'Платеж', '2015-12-08 13:59:16'),S
	(63, 2, 3, 456, 'Пополнение счета', '2015-12-08 13:59:25'),
	(64, 2, 3, 0, 'Блокировка счета', '2015-12-08 13:59:29'),
	(65, 2, 3, 100000, 'Платеж', '2015-12-08 16:07:47'),
	(66, 2, 3, 1, 'Пополнение счета', '2015-12-08 16:07:55'),
	(67, 2, 3, 0, 'Блокировка счета', '2015-12-08 16:08:02');